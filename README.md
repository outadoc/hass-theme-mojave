# hass-theme-mojave

A macOS Mojave-inspired dark theme for Home Assistant.

Add something like this in your `configuration.yaml`:

```yaml
frontend:
  themes: !include_dir_merge_named themes
```

Then, copy `mojave.yaml` to a `themes` folder in the same directory as your
`configuration.yaml`.
